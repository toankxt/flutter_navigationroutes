import 'package:flutter/material.dart';

final todos = List<ToDo>.generate(
    20, (i) => ToDo('Item $i', 'This is description Item $i'));

class ToDo {
  final String title;
  final String description;

  ToDo(this.title, this.description);
}

class TodosScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Todos'),
      ),
      body: ListView.builder(
          itemCount: todos.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(todos[index].title),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailItem(todo: todos[index])));
              },
            );
          }),
    );
  }
}

class DetailItem extends StatelessWidget {
  final ToDo todo;

  DetailItem({Key key, @required this.todo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(todo.title),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Text(todo.description),
      ),
    );
  }
}
