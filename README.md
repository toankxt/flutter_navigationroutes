# Flutter_NavigationRoutes

Thực hành lap 04 Navgation and Route in Flutter

# Getting Started

This project is a starting point for a Flutter application.

### **Chương trình chính**

```dart
import 'package:flutter/material.dart';

import 'views/arguments_view.dart';
import 'views/navigate-named_view.dart';
import 'views/navigate_view.dart';
import 'views/navigator_view.dart';
import 'views/return-data_view.dart';
import 'views/send-data_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigation Basics',
      routes: {'/second': (context) => SecondScreen()},
      home: HomePage(),
      onGenerateRoute: (settings) {
        if (settings.name == PassArgumentsScreen.routeName) {
          final ScreenArguments args = settings.arguments;
          return MaterialPageRoute(builder: (context) {
            return PassArgumentsScreen(
                title: args.title, message: args.message);
          });
        }
        assert(false, 'Need to implements ${settings.name}');
        return null;
      },
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lap 04 Navigator & Route'),
      ),
      body: Center(
        child: GestureDetector(
          onTap: () => {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SecondRoute()))
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  splashColor: Colors.blueAccent,
                  onPressed: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HeroScreen()))
                      },
                  child: Text('Navigator')),
              FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  splashColor: Colors.blueAccent,
                  onPressed: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FirstRoute()))
                      },
                  child: Text('Navigator new and back screen')),
              FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  splashColor: Colors.blueAccent,
                  onPressed: () => {Navigator.pushNamed(context, '/second')},
                  child: Text('Navigate with named routes')),
              FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  splashColor: Colors.blueAccent,
                  onPressed: () => {
                        Navigator.pushNamed(
                            context, PassArgumentsScreen.routeName,
                            arguments: ScreenArguments(
                                'Accept Arguments Screen',
                                'This message is extracted in the onGenerateRoute function.'))
                      },
                  child: Text('Arguments')),
              FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  splashColor: Colors.blueAccent,
                  onPressed: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomeScreen()))
                      },
                  child: Text('Return form data')),
              FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  splashColor: Colors.blueAccent,
                  onPressed: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TodosScreen()))
                      },
                  child: Text('Send data'))
            ],
          ),
        ),
      ),
    );
  }
}
```

## Kết quả

![Image1](screenshots/1.PNG)
![Image2](screenshots/2.PNG)
![Image3](screenshots/3.PNG)
![Image4](screenshots/4.PNG)
![Image5](screenshots/5.PNG)
![Image6](screenshots/6.PNG)
![Image7](screenshots/7.PNG)
![Image8](screenshots/8.PNG)
